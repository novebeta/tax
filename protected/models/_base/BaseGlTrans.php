<?php

/**
 * This is the model base class for the table "{{gl_trans}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "GlTrans".
 *
 * Columns in table "{{gl_trans}}" available as properties of the model,
 * followed by relations of table "{{gl_trans}}" available as properties of the model.
 *
 * @property integer $counter
 * @property integer $type
 * @property string $type_no
 * @property string $tran_date
 * @property string $memo_
 * @property double $amount
 * @property integer $user_id
 * @property string $account_code
 * @property string $store
 * @property string $tdate
 * @property integer $cf
 * @property integer $visible
 *
 * @property ChartMaster $accountCode
 */
abstract class BaseGlTrans extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{gl_trans}}';
	}

	public static function representingColumn() {
		return 'store';
	}

	public function rules() {
		return array(
			array('type_no, user_id, account_code, store, tdate', 'required'),
			array('type, user_id, cf, visible', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('type_no', 'length', 'max'=>10),
			array('account_code', 'length', 'max'=>15),
			array('store', 'length', 'max'=>20),
			array('tran_date, memo_', 'safe'),
			array('type, tran_date, memo_, amount, cf, visible', 'default', 'setOnEmpty' => true, 'value' => null),
			array('counter, type, type_no, tran_date, memo_, amount, user_id, account_code, store, tdate, cf, visible', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'accountCode' => array(self::BELONGS_TO, 'ChartMaster', 'account_code'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'counter' => Yii::t('app', 'Counter'),
			'type' => Yii::t('app', 'Type'),
			'type_no' => Yii::t('app', 'Type No'),
			'tran_date' => Yii::t('app', 'Tran Date'),
			'memo_' => Yii::t('app', 'Memo'),
			'amount' => Yii::t('app', 'Amount'),
			'user_id' => Yii::t('app', 'User'),
			'account_code' => Yii::t('app', 'Account Code'),
			'store' => Yii::t('app', 'Store'),
			'tdate' => Yii::t('app', 'Tdate'),
			'cf' => Yii::t('app', 'Cf'),
			'visible' => Yii::t('app', 'Visible'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('counter', $this->counter);
		$criteria->compare('type', $this->type);
		$criteria->compare('type_no', $this->type_no, true);
		$criteria->compare('tran_date', $this->tran_date, true);
		$criteria->compare('memo_', $this->memo_, true);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('account_code', $this->account_code);
		$criteria->compare('store', $this->store, true);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('cf', $this->cf);
		$criteria->compare('visible', $this->visible);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}