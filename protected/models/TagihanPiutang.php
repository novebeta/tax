<?php

Yii::import('application.models._base.BaseTagihanPiutang');

class TagihanPiutang extends BaseTagihanPiutang
{
	public function primaryKey()
	{
		return 'sales_id';
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}