<?php

Yii::import('application.models._base.BaseSales');

class Sales extends BaseSales
{
	public function primaryKey()
	{
		return 'sales_id';
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}