<?php

Yii::import('application.models._base.BaseTagihanUtang');

class TagihanUtang extends BaseTagihanUtang
{
	public function primaryKey()
	{
		return 'purchase_id';
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}