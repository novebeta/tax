<?php

Yii::import('application.models._base.BaseChartMaster');
class ChartMaster extends BaseChartMaster
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_child($coa, $inc_header = false)
    {
        $res = array();
        $criteria = new CDbCriteria();
        $criteria->addCondition("kategori = :account_code");
        $criteria->params = array(':account_code' => $coa);
        $chart = ChartMaster::model()->findAll($criteria);
        foreach ($chart as $c) {
            if ($c->header == 1) {
                if ($inc_header) {
                    $res[] = $c;
                }
            } else {
                $res[] = $c;
            }
            $childArr = Self::get_child($c->account_code, $inc_header);
            $res = array_merge($res, $childArr);
        }
        return $res;
    }
    public static function get_laba_rugi($from, $to, $store = null)
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND pgt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT pcm.account_code,pcm.account_name,
        IFNULL(Sum(pgt.amount),0) total FROM nscc_gl_trans AS pgt
            RIGHT JOIN nscc_chart_master pcm ON (pgt.account_code = pcm.account_code AND
        pgt.tran_date >= :from AND pgt.tran_date <= :to AND pgt.visible = 1)
        WHERE pcm.tipe = 'LR' $where
        GROUP BY pcm.account_code;");
        return $comm->queryAll(true, $param);
    }
    public static function get_neraca($from, $to, $store = null)
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND pgt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT pcm.account_code,pcm.account_name,pcm.header,pcm.saldo_normal,
    SUM(IF (pgt.tran_date < :from, pgt.amount, 0)) `before`,
    SUM(IF (pgt.amount >= 0 AND pgt.tran_date >= :from
		  AND pgt.tran_date <= :to, pgt.amount, 0)) `debit`,
    SUM(IF (pgt.amount < 0 AND pgt.tran_date >= :from
		  AND pgt.tran_date <= :to, ABS(pgt.amount), 0)) `kredit`,
    SUM(IF (pgt.tran_date <= :to, pgt.amount, 0)) `after`
    FROM nscc_gl_trans AS pgt
    RIGHT JOIN nscc_chart_master pcm ON (pgt.account_code = pcm.account_code AND pgt.visible = 1)
    WHERE pcm.tipe = 'NR' $where GROUP BY pcm.account_code;");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    public static function get_saldo_until($tgl, $account)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        Sum(pgt.amount) FROM psn_gl_trans AS pgt
        WHERE pgt.tran_date <= :tgl AND
        pgt.account_code = :account_code");
        return $comm->queryScalar(array(':tgl' => $tgl, ':account_code' => $account));
    }
}