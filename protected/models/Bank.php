<?php

Yii::import('application.models._base.BaseBank');
class Bank extends BaseBank
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_bank_cash()
    {
        return Bank::model()->findByPk(SysPrefs::get_val('kas_cabang'));
    }
    public static function get_bank_cash_while()
    {
        return Bank::model()->findByPk(SysPrefs::get_val('kas_cabang_sementara'));
    }
    public static function get_bank_cash_while_id()
    {
        $ret = self::get_bank_cash_while();
        if ($ret != null) {
            return $ret->bank_id;
        }
        throw new Exception(t('bank.fail.cash', 'app'));
        return -1;
    }
    public static function get_bank_cash_id()
    {
        $ret = self::get_bank_cash();
        if ($ret != null) {
            return $ret->bank_id;
        }
        throw new Exception(t('bank.fail.cash', 'app'));
        return -1;
    }
    public function is_bank_cash()
    {
        return self::get_bank_cash_id() == $this->bank_id;
    }
    public function get_cash_in($tgl)
    {
        $param = array(':tgl' => $tgl, ':bank_id' => $this->bank_id);
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.total), 0) total FROM nscc_kas ns
    WHERE ns.arus =1 AND ns.visible = 1 AND DATE(ns.tgl) = :tgl
    AND bank_id = :bank_id");
        return $comm->queryScalar($param);
    }
    public function get_cash_out($tgl)
    {
        $param = array(':tgl' => $tgl, ':bank_id' => $this->bank_id);
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.total), 0) total FROM nscc_kas ns
    WHERE ns.arus = -1 AND ns.visible = 1 AND DATE(ns.tgl) = :tgl
    AND bank_id = :bank_id");
        return $comm->queryScalar($param);
    }
    public function get_total_sales_payment($tgl)
    {
        $param = array(':tgl' => $tgl, ':bank' => $this->bank_id);
//        $amount = self::is_bank_cash() ? "np.amount - np.kembali" : "np.amount";
        $comm = Yii::app()->db->createCommand("SELECT
	IFNULL(SUM(np.amount - np.kembali), 0) total
    FROM nscc_salestrans ns
	INNER JOIN nscc_payment np
	 ON ns.salestrans_id = np.salestrans_id
    WHERE ns.bruto >= 0 AND DATE(ns.tgl) = :tgl AND np.bank_id = :bank");
        return $comm->queryScalar($param);
    }
    public function get_total_returnsales_payment_bank($tgl)
    {
        $where = "";
        $param = array(':tgl' => $tgl, ':bank' => $this->bank_id);
        $comm = Yii::app()->db->createCommand("SELECT
	IFNULL(SUM(np.amount), 0) total
    FROM nscc_salestrans ns
	INNER JOIN nscc_payment np
	 ON ns.salestrans_id = np.salestrans_id
    WHERE ns.bruto < 0 AND DATE(ns.tgl) = :tgl AND np.bank_id = :bank");
        return $comm->queryScalar($param);
    }
}