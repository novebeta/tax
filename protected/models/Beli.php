<?php

Yii::import('application.models._base.BaseBeli');

class Beli extends BaseBeli
{
	public function primaryKey()
	{
		return 'purchase_id';
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}