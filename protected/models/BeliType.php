<?php

Yii::import('application.models._base.BaseBeliType');

class BeliType extends BaseBeliType
{
	public function primaryKey()
	{
		return 'akun_with_coa_id';
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}