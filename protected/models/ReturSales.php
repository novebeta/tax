<?php

Yii::import('application.models._base.BaseReturSales');

class ReturSales extends BaseReturSales
{
	public function primaryKey()
	{
		return 'sales_id';
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}