<?php

Yii::import('application.models._base.BasePelunasanUtang');

class PelunasanUtang extends BasePelunasanUtang
{
    public function beforeValidate()
    {
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}