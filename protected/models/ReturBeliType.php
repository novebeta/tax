<?php

Yii::import('application.models._base.BaseReturBeliType');

class ReturBeliType extends BaseReturBeliType
{
	public function primaryKey()
	{
		return 'akun_with_coa_id';
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}