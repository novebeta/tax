<?php

Yii::import('application.models._base.BaseReturBeli');

class ReturBeli extends BaseReturBeli
{
	public function primaryKey()
	{
		return 'purchase_id';
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}