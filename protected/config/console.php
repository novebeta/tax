<?php
return array(
    'basePath' => dirname(__file__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'POS Natasha',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.giix-components.*',
        'application.vendors.*'
    ),
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=pos;port=3306',
            'emulatePrepare' => true,
            'tablePrefix' => 'nscc_',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
        ),
        'aes256' => array(
            'class' => 'application.extensions.aes256.Aes256',
            'privatekey_32bits_hexadecimal' => '9C9413969BD7524F4CD07FECDEE0ED185D1CB08A16DB4D56B14FF951A64E96AC',
        ),
        'Smtpmail' => array(
            'class' => 'application.extensions.smtpmail.PHPMailer',
            'Host' => "smtp.gmail.com",
            'Username' => 'nscc.sync@gmail.com',
            'Password' => 'zaq!@#$%',
            'Mailer' => 'smtp',
            'Port' => 587,
            'SMTPAuth' => true,
            'SMTPSecure' => 'tls',
        ),
    ),
    'params' => array(
        'Username' => 'nscc.sync@gmail.com',
        'Password' => 'zaq!@#$%',
        'system_title' => 'Point Of Sales Natasha',
        'system_subtitle' => '',
        'phpass' => array(
            'iteration_count_log2' => 8,
            'portable_hashes' => false,
        ),
    ),
);
