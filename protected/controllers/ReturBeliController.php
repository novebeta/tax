<?php
class ReturBeliController extends GxController
{
    public function actionCreate()
    {
        $model = new Purchases;
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data berhasil disimpan.";
            $_POST['Beli']['ppn'] = 0;
            $ppn = SysPrefs::get_val('ppn');
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $gl = new GL();
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Beli'][$k] = $v;
                }
                if ($_POST['Beli']['ppn'] == 1) {
                    $_POST['Beli']['tax'] = $ppn;
                    $_POST['Beli']['amount'] = $_POST['Beli']['total'] / ((100 + $ppn) / 100);
                    $_POST['Beli']['tax_rp'] = $_POST['Beli']['total'] - $_POST['Beli']['amount'];
                } else {
                    $_POST['Beli']['tax'] = 0;
                    $_POST['Beli']['tax_rp'] = 0;
                    $_POST['Beli']['amount'] = $_POST['Beli']['total'];
                }
                $_POST['Beli']['total'] = -$_POST['Sales']['total'];
                $_POST['Beli']['tax_rp'] = -$_POST['Sales']['tax_rp'];
                $_POST['Beli']['amount'] = -$_POST['Sales']['amount'];
                $_POST['Beli']['lunas'] = 1;
                $ref = new Reference();
                $docref = $ref->get_next_reference(BELI);
                $_POST['Beli']['doc_ref'] = $docref;
                $model->attributes = $_POST['Beli'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Retur Pembelian')) . CHtml::errorSummary($model));
                }
                /* @var $beli Purchases */
                $beli = Purchases::model()->find("doc_ref = :doc_ref", array(":doc_ref" => $model->doc_ref_other));
                if($beli == null){
                    throw new Exception("Doc. Ref Pembelian tidak ditemukan.");
                }elseif($beli->total < abs($model->total)){
                    throw new Exception("Nilai Pembelian lebih kecil dari Retur Pembelian.");
                }elseif($beli->total == abs($model->total)){
                    $beli->lunas = 1;
                    if (!$beli->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Pembelian')) . CHtml::errorSummary($model));
                    }
                }
                $gl->add_gl(RETURBELI, $model->purchase_id, $model->tgl, $model->doc_ref,
                    SysPrefs::get_val('coa_persediaan'),
                    "Retur Pembelian " . $model->doc_ref, "Retur Pembelian " . $model->doc_ref, $model->total, 0,
                    $model->store);
                $gl->add_gl(RETURBELI, $model->purchase_id, $model->tgl, $model->doc_ref,
                    $model->akunWithCoa->account_code,
                    "Retur Pembelian " . $model->doc_ref, "Retur Pembelian " . $model->doc_ref, -$model->amount, 0,
                    $model->store);
                if ($model->ppn == 1) {
                    $gl->add_gl(RETURBELI, $model->purchase_id, $model->tgl, $model->doc_ref,
                        SysPrefs::get_val('coa_ppn_beli'),
                        "Retur Pembelian " . $model->doc_ref, "Retur Pembelian " . $model->doc_ref, -$model->tax_rp, 0,
                        $model->store);
                }
                $gl->validate();
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'ReturBeli');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['ReturBeli'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['ReturBeli'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->purchase_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->purchase_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'ReturBeli')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = ReturBeli::model()->findAll($criteria);
        $total = ReturBeli::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}