<?php
Yii::import('application.components.U');
class SiteController extends Controller
{
//    public function accessRules() {
//        return array(
//            array('allow',
//                'users' => array('*'),
//                'actions' => array('login'),
//            )           
//        );
//    }
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array('class' => 'CViewAction',),
        );
    }
    public function actionDateDiff()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $date1 = $_POST['dari'];
            $date2 = $_POST['sampai'];
            $diff = abs(strtotime($date2) - strtotime($date1));
            $years = floor($diff / (365 * 60 * 60 * 24));
            echo CJSON::encode(
                array(
                    'status' => true,
                    'msg' => $years
                ));
            Yii::app()->end();
        }
    }
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->layout = 'main';
        $this->render('index');
    }
    public function actionGetDateTime()
    {
        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array(
                'success' => true,
                'datetime' => date('Y-m-d H:i:s')
            ));
            Yii::app()->end();
        }
    }
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }
    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" . "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" . "Content-type: text/plain; charset=UTF-8";
                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact',
                    'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
    public function actionLogin()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->layout = 'login';
            $this->render('login');
        } else {
            $model = new LoginForm;
            $loginUsername = isset($_POST["loginUsername"]) ? $_POST["loginUsername"]
                : "";
            $loginPassword = isset($_POST["loginPassword"]) ? $_POST["loginPassword"]
                : "";
            if ($loginUsername != "") {
                //$model->attributes = $_POST['LoginForm'];
                $model->username = $loginUsername;
                $model->password = $loginPassword;
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login()) {
                    echo "{success: true}";
                } else {
                    echo "{success: false, errors: { reason: 'Login failed. Try again.' }}";
                }
            } else {
                echo "{success: false, errors: { reason: 'Login failed. Try again' }}";
            }
        }
    }
    public function actionLoginOverride()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect('/');
        } else {
            $count = Users::get_override($_POST["loginUsername"], $_POST["loginPassword"]);
            if ($count) {
                echo CJSON::encode(array(
                    'success' => true,
                    'msg' => $count
                ));
            } else {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => "You don't have permission."
                ));
            }
        }
    }
    public function actionGenerate()
    {
        $templatePath = './css/silk_v013/icons';
        $files = scandir($templatePath);
        $txt = "";
        foreach ($files as $file) {
            if (is_file($templatePath . '/' . $file)) {
                $basename = explode(".", $file);
                $name = $basename[0];
                $txt .= ".silk13-$name { background-image: url(icons/$file) !important; background-repeat: no-repeat; }\n";
            }
        }
        $myFile = "silk013.css";
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $txt);
        fclose($fh);
    }
    public function actionTree()
    {
        $user = Users::model()->findByPk(user()->getId());
        $menu = new MenuTree($user->security_roles_id);
        $data = $menu->get_menu();
        Yii::app()->end($data);
    }
    public function actionBackupAll()
    {
        if (!Yii::app()->request->isPostRequest) {
            $this->redirect(bu());
        }
        $conn = Yii::app()->db;
        $user = $conn->username;
        $pass = $conn->password;
        if (preg_match('/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result)) {
            list($all, $host, $db, $port) = $result;
        }
        $dir_backup = dirname(Yii::app()->request->scriptFile) . "\\backup\\";
        $files = scandir($dir_backup);
        foreach ($files as $file) {
            if (is_file("$dir_backup\\$file")) {
                unlink("$dir_backup\\$file");
            }
        }
        $backup_file = $dir_backup . $db . date("Y-m-d-H-i-s") . '.pos';
        $mysqldump = dirname(Yii::app()->request->scriptFile) . "\\mysqldump";
        $gzip = dirname(Yii::app()->request->scriptFile) . "\\gzip";
        $command = "$mysqldump --opt -h $host -u $user --password=$pass -P $port " .
            "$db > $backup_file";
        system($command);
        $size = filesize($backup_file);
        if ($size > 0) {
            $command = "$gzip -9 $backup_file";
            system($command);
            $size = filesize("$backup_file.gz");
            $bu = bu() . "/backup/" . basename("$backup_file.gz");
            header("Location: $bu");
        }
    }
    public function actionRestoreAll()
    {
        if (!Yii::app()->request->isPostRequest) {
            $this->redirect(bu());
        }
        $conn = Yii::app()->db;
        $user = $conn->username;
        $pass = $conn->password;
        if (preg_match('/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result)) {
            list($all, $host, $db, $port) = $result;
        }
        $dir_backup = dirname(Yii::app()->request->scriptFile) . "\\backup\\";
        if (isset($_FILES["filename"])) { // it is recommended to check file type and size here
            if ($_FILES["filename"]["error"] > 0) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => $_FILES["file"]["error"]
                ));
            } else {
                $backup_file = $dir_backup . $_FILES["filename"]["name"];
                move_uploaded_file($_FILES["filename"]["tmp_name"], $backup_file);
                $mysql = dirname(Yii::app()->request->scriptFile) . "\\mysql";
                $gzip = dirname(Yii::app()->request->scriptFile) . "\\gzip";
                $command = "$gzip -d $backup_file";
                system($command);
                $backup_file = substr($backup_file, 0, -3);
                if (!file_exists($backup_file)) {
                    echo CJSON::encode(array(
                        'success' => false,
                        'msg' => "Failed restore file " . $_FILES["file"]["name"]
                    ));
                } else {
                    Yii::app()->db->createCommand("DROP DATABASE $db")
                        ->execute();
                    Yii::app()->db->createCommand("CREATE DATABASE IF NOT EXISTS $db")
                        ->execute();
                    $command = "$mysql -h $host -u $user --password=$pass -P $port " .
                        "$db < $backup_file";
                    system($command);
                    echo CJSON::encode(array(
                        'success' => true,
                        'msg' => "Succefully restore file " . $_FILES["file"]["name"]
                    ));
                }
                Yii::app()->end();
            }
        }
    }
    public function actionDeleteTransAll()
    {
        if (!Yii::app()->request->isPostRequest) {
            $this->redirect(bu());
        }
        Yii::app()->db->createCommand("
        /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
        TRUNCATE TABLE nscc_bank_trans;
        TRUNCATE TABLE nscc_beauty_services;
        TRUNCATE TABLE nscc_kas_detail;
        TRUNCATE TABLE nscc_kas;
        TRUNCATE TABLE nscc_salestrans_details;
        TRUNCATE TABLE nscc_salestrans;
        TRUNCATE TABLE nscc_stock_moves;
        TRUNCATE TABLE nscc_tender_details;
        TRUNCATE TABLE nscc_tender;
        TRUNCATE TABLE nscc_transfer_item_details;
        TRUNCATE TABLE nscc_transfer_item;
        TRUNCATE TABLE nscc_gl_trans;
        TRUNCATE TABLE nscc_comments;
        TRUNCATE TABLE nscc_payment;
        TRUNCATE TABLE nscc_refs;
        TRUNCATE TABLE nscc_sync;
        TRUNCATE TABLE nscc_upload_log;
        TRUNCATE TABLE nscc_pelunasan_utang_detil;
        TRUNCATE TABLE nscc_pelunasan_utang;
        /*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
        ")->execute();
        echo CJSON::encode(array(
            'success' => true,
            'msg' => "Succefully delete all transaction "
        ));
    }
    public function actionCustNars()
    {
    }
    public function actionSync()
    {
        session_write_close();
        $stat = SysPrefs::model()->find('name_ = :name AND store =:store',
            array(':name' => 'sync_active', ':store' => STOREID));
        if ($stat->value_ != "0") {
            $date = strtotime($stat->value_);
            $date2 = time();
            $subTime = $date2 - $date;
            $h = $subTime / (60 * 60);
            if ($h > 1) {
                $stat->value_ = '0';
                $stat->save();
            }
            $msg = 'other client already sync. Diff time ' . $h;
        } else {
            $stat->value_ = date('Y-m-d H:i:s');
            $stat->save();
            $s = new SyncData();
            $msg = $s->sync();
            $stat->value_ = '0';
            $stat->save();
        }
        $report = json_encode(array(
                'type' => 'event',
                'name' => 'message',
                'time' => date('g:i:s a'),
                'data' => $msg
            ),
            JSON_PRETTY_PRINT);
        echo $report;
    }
    public function actionTestEncrypt()
    {
        $global = array(
            'SecurityRoles',
            'Gol',
            'TransTipe',
            'StatusCust',
            'ChartMaster',
            'Supplier',
            'Kategori',
            'Negara',
            'Provinsi',
            'Kota',
            'Kecamatan',
            'Store'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $upload = array();
        $content = array();
        foreach ($global as $modelName) {
            $model = new $modelName();
            $tables = $model::model()->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
        }
        $to = yiiparam('Username');
        echo mailsend($to, $to, 'GLOBAL-' . sha1(STOREID . "^" . date('Y-m-d H:i:s')),
            json_encode(array(
                'from' => STOREID,
                'date' => date('Y-m-d H:i:s'),
                'content' => array($content)
            ), JSON_PRETTY_PRINT),
            bzcompress(Encrypt(CJSON::encode($upload)), 9));
    }
    public function actionCheckEmail()
    {
        $username = 'nscc.sync@gmail.com';
        $password = 'zaq!@#$%';
        $imapmainbox = "TSBK01";
        $messagestatus = "ALL";
        $imapaddress = "{imap.gmail.com:993/imap/ssl}";
        $hostname = $imapaddress . $imapmainbox;
        $connection = imap_open($hostname, $username,
            $password) or die('Cannot connect to Gmail: ' . imap_last_error());
        $emails = imap_search($connection, $messagestatus);
        $totalemails = imap_num_msg($connection);
        echo "Total Emails: " . $totalemails . "<br>";
        if ($emails) {
            //sort emails by newest first
            //rsort($emails);
            //loop through every email int he inbox
            foreach ($emails as $email_number) {
                //grab the overview and message
                $header = imap_fetch_overview($connection, $email_number, 0);
                //Because attachments can be problematic this logic will default to skipping the attachments
                $structure = imap_fetchstructure($connection, $email_number);
                $attachments = array();
                if (isset($structure->parts) && count($structure->parts)) {
                    for ($i = 0; $i < count($structure->parts); $i++) {
                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename' => '',
                            'name' => '',
                            'attachment' => ''
                        );
                        if ($structure->parts[$i]->ifdparameters) {
                            foreach ($structure->parts[$i]->dparameters as $object) {
                                if (strtolower($object->attribute) == 'filename') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = $object->value;
                                }
                            }
                        }
                        if ($structure->parts[$i]->ifparameters) {
                            foreach ($structure->parts[$i]->parameters as $object) {
                                if (strtolower($object->attribute) == 'name') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = $object->value;
                                }
                            }
                        }
                        if ($attachments[$i]['is_attachment']) {
                            $attachments[$i]['attachment'] = imap_fetchbody($connection, $email_number, $i + 1);
                            if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    } // for($i = 0; $i < count($structure->parts); $i++)
                } // if(isset($structure->parts) && count($structure->parts))
                $status = ($header[0]->seen ? 'read' : 'unread');
                $subject = $header[0]->subject;
                $from = $header[0]->from;
                $date = $header[0]->date;
                echo "status: " . $status . "<br>";
                echo "subject: " . $subject . "<br>";
                echo "from: " . $from . "<br>";
                echo "date: " . $date . "<br>";
                $message = imap_fetchbody($connection, $email_number, 1.1);
                if ($message == "") { // no attachments is the usual cause of this
                    $message = imap_fetchbody($connection, $email_number, 1);
                }
                echo "body: " . $message . "<br>";
                if (count($attachments) != 0) {
                    foreach ($attachments as $at) {
                        if ($at[is_attachment] == 1) {
//                            file_put_contents($at[filename], $at[attachment]);
                            echo "Attachment: " . Decrypt(bzdecompress($at[attachment])) . "<br>";
                        }
                    }
                }
                echo "<hr><br>";
            }
        }
        imap_close($connection);
    }
    public function actionPoll()
    {
        echo json_encode(array(
            'type' => 'event',
            'name' => 'message',
            'data' => 'Successfully polled at: ' . date('g:i:s a')
        ));
    }
    public function actionDecrypt()
    {
        $this->render('Decrypt');
        if (isset($_POST['content'])) {
            echo Decrypt($_POST['content']);
        }
    }
    public function actionClosing()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $year = $_POST['year'];
            $tgl = $year . '-12-31';
            $store = $_POST['store'];
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $gltrans = Yii::app()->db->createCommand("
                select ngl.account_code,IFNULL(SUM(ngl.amount),0) amount
                FROM nscc_gl_trans ngl
                WHERE ngl.tran_date <= :tgl AND ngl.visible = 1 AND ngl.store = :store
                GROUP BY ngl.account_code")
                    ->queryAll(true, array(':tgl' => $tgl, ':store' => $store));
                $banktrans = Yii::app()->db->createCommand("
                SELECT nbt.bank_id,SUM(nbt.amount) amount
                FROM nscc_bank_trans nbt
                WHERE nbt.tgl <= :tgl AND nbt.visible = 1 AND nbt.store = :store
                GROUP BY nbt.bank_id")
                    ->queryAll(true, array(':tgl' => $tgl, ':store' => $store));
                $stockmove = Yii::app()->db->createCommand("
                SELECT nsm.barang_id,SUM(nsm.qty) qty
                from nscc_stock_moves nsm
                WHERE nsm.tran_date <= :tgl AND nsm.store = :store
                GROUP BY nsm.barang_id")
                    ->queryAll(true, array(':tgl' => $tgl, ':store' => $store));
                Yii::app()->db->createCommand("
                DELETE FROM nscc_audit WHERE tgl <= :tgl AND store = :store;
                DELETE FROM nscc_bank_trans WHERE tgl <= :tgl AND store = :store;
                DELETE FROM nscc_comments WHERE date_ <= :tgl;
                DELETE FROM nscc_gl_trans WHERE tran_date <= :tgl AND store = :store;
                DELETE FROM nscc_kas WHERE tgl <= :tgl AND store = :store;
                DELETE FROM nscc_salestrans WHERE tgl <= :tgl AND store = :store;
                DELETE FROM nscc_stock_moves WHERE tran_date <= :tgl AND store = :store;
                DELETE FROM nscc_tender WHERE tgl <= :tgl AND store = :store;
                DELETE FROM nscc_transfer_item WHERE tgl <= :tgl AND store = :store;")
                    ->execute(array(':tgl' => $tgl, ':store' => $store));
                foreach ($gltrans as $row) {
                    if ($row['amount'] == 0) {
                        continue;
                    }
                    $gl = new GlTrans;
                    $gl->type = SALDO_AWAL;
                    $gl->type_no = $year;
                    $gl->tran_date = $tgl;
                    $gl->memo_ = "Closing $year";
                    $gl->amount = $row['amount'];
                    $gl->id_user = Yii::app()->user->getId();
                    $gl->account_code = $row['account_code'];
                    $gl->store = $store;
                    $gl->tdate = new CDbExpression('NOW()');
                    $gl->cf = 0;
                    $gl->up = 1;
                    $gl->visible = 1;
                    if (!$gl->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'General Journal')) . CHtml::errorSummary($gl));
                    }
                }
                foreach ($banktrans as $row) {
                    if ($row['amount'] == 0) {
                        continue;
                    }
                    $bt = new BankTrans;
                    $bt->ref = "-";
                    $bt->type_ = SALDO_AWAL;
                    $bt->trans_no = $year;
                    $bt->tgl = $tgl;
                    $bt->amount = $row['amount'];
                    $bt->id_user = Yii::app()->user->getId();
                    $bt->tdate = new CDbExpression('NOW()');
                    $bt->bank_id = $row['bank_id'];
                    $bt->store = $store;
                    $bt->up = 1;
                    $bt->visible = 1;
                    if (!$bt->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Bank Trans')) . CHtml::errorSummary($bt));
                    }
                }
                foreach ($stockmove as $row) {
                    if ($row['qty'] == 0) {
                        continue;
                    }
                    $barang = Barang::model()->findByPk($row['barang_id']);
//                    $barang = new Barang();
                    $stm = new StockMoves;
                    $stm->reference = "-";
                    $stm->type_no = SALDO_AWAL;
                    $stm->trans_no = $year;
                    $stm->tran_date = $tgl;
                    $stm->price = $barang->get_price($store);
                    $stm->qty = $row['qty'];
                    $stm->id_user = Yii::app()->user->getId();
                    $stm->tdate = new CDbExpression('NOW()');
                    $stm->barang_id = $row['barang_id'];
                    $stm->store = $store;
                    $stm->up = 1;
                    if (!$stm->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Bank Trans')) . CHtml::errorSummary($stm));
                    }
                }
                /* @var $pelunasan_utang PelunasanUtang[] */
                $pelunasan_utang = PelunasanUtang::model()->findAll('tgl <= :tgl AND store = :store',
                    array(":tgl" => $tgl, ':store' => $store));
                foreach($pelunasan_utang as $pelunasan){
                    foreach ($pelunasan->pelunasanUtangDetils as $detail) {
                        /* @var $retur_purchase TransferItem[] */
                        $retur_purchase = TransferItem::model()->findAll('doc_ref_other = :doc_ref',
                            array(':doc_ref'=>$detail->transferItem->doc_ref));
                        foreach($retur_purchase as $retur){
                            TransferItemDetails::model()->deleteAll('transfer_item_id = :transfer_item_id',
                                array(':transfer_item_id' => $retur->transfer_item_id));
                            $retur->delete();
                        }
                        TransferItemDetails::model()->deleteAll('transfer_item_id = :transfer_item_id',
                            array(':transfer_item_id' => $detail->transferItem->transfer_item_id));
                        $detail->transferItem->delete();
                        $detail->delete();
                    }
                    $pelunasan->delete();
                }
                $transaction->commit();
                $msg = 'Closing successfuly...';
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $msg = $ex->getMessage();
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
}
