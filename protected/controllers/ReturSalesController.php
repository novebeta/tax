<?php
class ReturSalesController extends GxController
{
    public function actionCreate()
    {
        $model = new Jual;
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data berhasil disimpan.";
            $_POST['Sales']['ppn'] = 0;
            $ppn = SysPrefs::get_val('ppn');
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $gl = new GL();
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Sales'][$k] = $v;
                }
                if ($_POST['Sales']['ppn'] == 1) {
                    $_POST['Sales']['tax'] = $ppn;
                    $_POST['Sales']['tax_rp'] = $_POST['Sales']['total'] / (100 / (100 + $ppn));
                    $_POST['Sales']['amount'] = $_POST['Sales']['total'] - $_POST['Sales']['tax_rp'];
                } else {
                    $_POST['Sales']['tax'] = 0;
                    $_POST['Sales']['tax_rp'] = 0;
                    $_POST['Sales']['amount'] = $_POST['Sales']['total'];
                }
                $_POST['Sales']['total'] = -$_POST['Sales']['total'];
                $_POST['Sales']['tax_rp'] = -$_POST['Sales']['tax_rp'];
                $_POST['Sales']['amount'] = -$_POST['Sales']['amount'];
                $_POST['Sales']['lunas'] = 1;
                $ref = new Reference();
                $docref = $ref->get_next_reference(RETURJUAL);
                $_POST['Sales']['doc_ref'] = $docref;
                $model->attributes = $_POST['Sales'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Penjualan')) . CHtml::errorSummary($model));
                }
                /* @var $jual Jual */
                $jual = Jual::model()->find("doc_ref = :doc_ref_sales", array(":doc_ref_sales" => $model->doc_ref_sales));
                if($jual == null){
                    throw new Exception("Doc. Ref Penjualan tidak ditemukan.");
                }elseif($jual->total < abs($model->total)){
                    throw new Exception("Nilai Penjualan lebih kecil dari Retur Penjualan.");
                }elseif($jual->total == abs($model->total)){
                    $jual->lunas = 1;
                    if (!$jual->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Penjualan')) . CHtml::errorSummary($model));
                    }
                }
                $gl->add_gl(RETURJUAL, $model->sales_id, $model->tgl, $model->doc_ref, SysPrefs::get_val('coa_piutang'),
                    "Penjualan " . $model->doc_ref, "Penjualan " . $model->doc_ref, $model->total, 0, $model->store);
                $gl->add_gl(RETURJUAL, $model->sales_id, $model->tgl, $model->doc_ref,
                    $model->akunWithCoa->account_code,
                    "Penjualan " . $model->doc_ref, "Penjualan " . $model->doc_ref, -$model->amount, 0, $model->store);
                if ($model->ppn == 1) {
                    $gl->add_gl(RETURJUAL, $model->sales_id, $model->tgl, $model->doc_ref, SysPrefs::get_val('coa_ppn'),
                        "Penjualan " . $model->doc_ref, "Penjualan " . $model->doc_ref, -$model->tax_rp, 0,
                        $model->store);
                }
                $gl->validate();
                $ref->save(RETURJUAL, $model->sales_id, $docref);
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'ReturSales');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['ReturSales'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['ReturSales'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->sales_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->sales_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'ReturSales')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = ReturSales::model()->findAll($criteria);
        $total = ReturSales::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}