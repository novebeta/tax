<?php

/**
 * Created by novebeta.
 * Date: 9/23/12
 * Time: 12:03 PM
 */
class Reference
{
    function get_next_reference($type)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type_id =" . $type);
        $model = SysTypes::model()->find($criteria);
        return $model->next_reference;
    }

    function save_next_reference($type, $reference)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type_id =" . $type);
        $model = SysTypes::model()->find($criteria);
        $model->next_reference = trim($reference);
        if (!$model->save())
            throw new Exception("Gagal menyimpan referensi selanjutnya.");
    }

    public static function get_reference($type, $type_no)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type_ = :type_");
        $criteria->addCondition("type_no = :type_no");
        $criteria->params = array(':type_' => $type, ':type_no' => $type_no);
        $model = Refs::model()->find($criteria);
        if ($model == null) {
            return null;
        }
        return $model->reference;
    }

    function update_reference($type, $id, $reference)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type_ = :type_");
        $criteria->addCondition("type_no = :type_no");
        $criteria->params = array(':type_' => $type, ':type_no' => $id);
        $model = Refs::model()->find($criteria);
        if ($model == null) {
            $model = new Refs();
            $model->type_ = $type;
            $model->type_no = $id;
        }
        $model->reference = $reference;
        if (!$model->save())
            throw new Exception("Gagal menyimpan referensi.");
    }

    function save($type, $id, $reference)
    {
        $next = '';
        if ($type != CUSTOMER) {
            $this->update_reference($type, $id, $reference); // store in refs table
            $next = $this->_increment($reference); // increment default
        } else {
            $next = $this->_customers($reference);
        }
        $this->save_next_reference($type, $next);
    }

    function save_reset($type, $reference)
    {
        //$this->update_reference($type, $id, $reference); // store in refs table
        $next = $this->_reset($reference); // increment default
        $this->save_next_reference($type, $next);
    }

    function _increment($reference, $back = false)
    {
        // New method done by Pete. So f.i. WA036 will increment to WA037 and so on.
        // If $reference contains at least one group of digits,
        // extract first didgits group and add 1, then put all together.
        // NB. preg_match returns 1 if the regex matches completely
        // also $result[0] holds entire string, 1 the first captured, 2 the 2nd etc.
        //
        if (preg_match('/^(\w{5})(\D*?)(\d{4})(\d+)(.*)/', $reference, $result) == 1) {
            list($all, $store, $prefix, $year, $number, $postfix) = $result;
            $dig_count = strlen($number); // How many digits? eg. 0003 = 4
            $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
            $yearnew = date("Y");
            if ($yearnew != $year) {
                $year = $yearnew;
                $number = 1;
            }
            $val = intval($number + ($back ? ($number < 1 ? 0 : -1) : 1));
            $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            return $store . $prefix . $year . $nextval . $postfix;
        } else
            return $reference;
    }

    function _customers($reference, $back = false)
    {
        // New method done by Pete. So f.i. WA036 will increment to WA037 and so on.
        // If $reference contains at least one group of digits,
        // extract first didgits group and add 1, then put all together.
        // NB. preg_match returns 1 if the regex matches completely
        // also $result[0] holds entire string, 1 the first captured, 2 the 2nd etc.
        //
        if (preg_match('/^(\w{5})(\d+)(.*)/', $reference, $result) == 1) {
            list($all, $prefix, $number, $postfix) = $result;
            $dig_count = strlen($number); // How many digits? eg. 0003 = 4
            $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
            $val = intval($number + ($back ? ($number < 1 ? 0 : -1) : 1));
            $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            return $prefix . $nextval . $postfix;
        } else
            return $reference;
    }

    function _reset($reference, $back = false)
    {
        // New method done by Pete. So f.i. WA036 will increment to WA037 and so on.
        // If $reference contains at least one group of digits,
        // extract first didgits group and add 1, then put all together.
        // NB. preg_match returns 1 if the regex matches completely
        // also $result[0] holds entire string, 1 the first captured, 2 the 2nd etc.
        //
        if (preg_match('/^(\D*?)(\d{4})(\d+)(.*)/', $reference, $result) == 1) {
            list($all, $prefix, $year, $number, $postfix) = $result;
            $year = date("Y");
            $dig_count = strlen($number); // How many digits? eg. 0003 = 4
            $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
            $val = intval(1);
            $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            return $prefix . $year . $nextval . $postfix;
        } else
            return $reference;
    }
}