<?php
class MenuTree
{
    var $security_role;
    var $menu_users = "{text: 'User Manajement',
                                id: 'jun.UsersGrid',
                                leaf: true
                                },";
    var $security = "{text: 'Security Roles',
                                id: 'jun.SecurityRolesGrid',
                                leaf: true
                                },";
    function __construct($id)
    {
        $role = SecurityRoles::model()->findByPk($id);
        $this->security_role = explode(",", $role->sections);
    }
    function getChildMaster()
    {
        $child = "";
        $child .= in_array(105, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Bank',
                            id: 'jun.BankGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(115, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Store',
                            id: 'jun.StoreGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(116, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Customers',
                            id: 'jun.CustomersGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(116, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Supplier',
                            id: 'jun.SupplierGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(117, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Chart Of Account',
                            id: 'jun.ChartMasterGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(117, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Tipe Penjualan',
                            id: 'jun.JualTypeGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(117, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Tipe Retur Penjualan',
                            id: 'jun.ReturJualTypeGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(117, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Tipe Pembelian',
                            id: 'jun.BeliTypeGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(205, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Tipe Retur Pembelian',
                            id: 'jun.ReturBeliTypeGrid',
                            leaf: true
                    }," : '';
        return $child;
    }
    function getMaster($child)
    {
        if ($child == null)
            return "";
        return "{
                    text: 'Master',
                    expanded: false,
                    children:[
                    $child
                    ]
                },";
    }
    function getTransaction($child)
    {
        if ($child == null)
            return "";
        return "{
                    text: 'Transaction',
                    expanded: false,
                    children:[
                    $child
                    ]
                },";
    }
    function getChildTransaction()
    {
        $child = "";
        $child .= in_array(205, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Penjualan',
                            id: 'jun.SalesGrid',
                            leaf: true
                    }," : '';
        $child .= in_array(206, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Retur Penjualan',
                            id: 'jun.ReturSalesGrid',
                            leaf: true
                    }," : '';
        $child .= in_array(206, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Pelunasan Piutang',
                            id: 'jun.PelunasanPiutangGrid',
                            leaf: true
                    }," : '';
        $child .= in_array(205, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Pembelian',
                            id: 'jun.BeliGrid',
                            leaf: true
                    }," : '';
        $child .= in_array(206, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Retur Pembelian',
                            id: 'jun.ReturBeliGrid',
                            leaf: true
                    }," : '';
        $child .= in_array(206, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Pelunasan Utang',
                            id: 'jun.PelunasanUtangGrid',
                            leaf: true
                    }," : '';
        $child .= in_array(211, $this->security_role) && !Users::is_audit() ? "{
                            text: 'General Journal',
                            id: 'jun.JurnalUmum',
                            leaf: true
                    }," : '';
        $child .= in_array(212, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Cash/Bank Deposit',
                            id: 'jun.KasGridPusat',
                            leaf: true
                    }," : '';
        $child .= in_array(213, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Cash/Bank Payment',
                            id: 'jun.KasGridPusatOut',
                            leaf: true
                    }," : '';
        $child .= in_array(214, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Cash/Bank Transfer',
                            id: 'jun.BankTransGrid',
                            leaf: true
                    }," : '';
        $child .= in_array(215, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Generate Profit Lost',
                            id: 'jun.GenerateLabaRugi',
                            leaf: true
                    }," : '';
        $child .= in_array(216, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Yearly Closing',
                            id: 'jun.ClosingStore',
                            leaf: true
                    }," : '';
        return $child;
    }
    function getReport($child)
    {
        if ($child == null)
            return "";
        return "{
                    text: 'Report',
                    expanded: false,
                    children:[
                    $child
                    ]
                },";
    }
    function getChildReport()
    {
        $child = "";
        $child .= in_array(314, $this->security_role) && !Users::is_audit() ? "{
                            text: 'General Ledger',
                            id: 'jun.ReportGeneralLedger',
                            leaf: true
                    }," : '';
        $child .= in_array(315, $this->security_role) && !Users::is_audit() ? "{
                            text: 'General Journal',
                            id: 'jun.ReportGeneralJournal',
                            leaf: true
                    }," : '';
        $child .= in_array(318, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Profit Lost',
                            id: 'jun.ReportLabaRugi',
                            leaf: true
                    }," : '';
        $child .= in_array(319, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Balance Sheet',
                            id: 'jun.ReportBalanceSheet',
                            leaf: true
                    }," : '';
        $child .= in_array(320, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Neraca',
                            id: 'jun.ReportNeraca',
                            leaf: true
                    }," : '';
        return $child;
    }
    function getAdministration($child)
    {
        if ($child == null)
            return "";
        return "{
                    text: 'Administration',
                    expanded: false,
                    children:[
                    $child
                    ]
                },";
    }
    function getChildAdministration()
    {
        $child = "";
        $child .= in_array(400, $this->security_role) && !Users::is_audit() ? "{
                            text: 'User Management',
                            id: 'jun.UsersGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(401, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Security Roles',
                            id: 'jun.SecurityRolesGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(402, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Backup / Restore',
                            id: 'jun.BackupRestoreWin',
                            leaf: true
                        }," : '';
        $child .= in_array(403, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Import',
                            id: 'jun.ImportXlsx',
                            leaf: true
                        }," : '';
        return $child;
    }
    function getGeneral()
    {
        $username = Yii::app()->user->name;
        $child = "";
        $child .= in_array(000, $this->security_role) ? "{
                            text: 'Change Password',
                            id: 'jun.PasswordWin',
                            leaf: true
                        }," : '';
        $child .= in_array(001, $this->security_role) ? "{
                            text: 'Logout ($username)',
                            id: 'logout',
                            leaf: true
                        }," : '';
        return $child;
    }
    public function get_menu()
    {
        $username = Yii::app()->user->name;
        $data = "[";
        $data .= self::getMaster(self::getChildMaster());
        $data .= self::getTransaction(self::getChildTransaction());
        $data .= self::getReport(self::getChildReport());
        $data .= self::getAdministration(self::getChildAdministration());
        $data .= self::getGeneral();
        $data .= "]";
        return $data;
    }
}
