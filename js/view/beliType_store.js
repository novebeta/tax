jun.BeliTypestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BeliTypestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BeliTypeStoreId',
            url: 'BeliType',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'akun_with_coa_id'},
                {name: 'nama_akun'},
                {name: 'type_'},
                {name: 'account_code'}
            ]
        }, cfg));
    }
});
jun.rztBeliType = new jun.BeliTypestore();
jun.rztBeliTypeCmp = new jun.BeliTypestore();
jun.rztBeliTypeLib = new jun.BeliTypestore();
//jun.rztBeliType.load();
