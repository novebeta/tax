jun.Customersstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Customersstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CustomersStoreId',
            url: 'Customers',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'customer_id'},
                {name: 'customer_name'}
            ]
        }, cfg));
    }
});
jun.rztCustomers = new jun.Customersstore();
jun.rztCustomersLib = new jun.Customersstore();
jun.rztCustomersCmp = new jun.Customersstore();
//jun.rztCustomers.load();
