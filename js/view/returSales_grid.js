jun.ReturSalesGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Retur Penjualan",
    id: 'docs-jun.ReturSalesGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Ref. Dok',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100
        },
        {
            header: 'PPN',
            sortable: true,
            resizable: true,
            dataIndex: 'ppn',
            width: 100
        },
        {
            header: 'Store',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
        /*
         {
         header:'store',
         sortable:true,
         resizable:true,
         dataIndex:'store',
         width:100
         },
         {
         header:'tax',
         sortable:true,
         resizable:true,
         dataIndex:'tax',
         width:100
         },
         {
         header:'tax_rp',
         sortable:true,
         resizable:true,
         dataIndex:'tax_rp',
         width:100
         },
         {
         header:'amount',
         sortable:true,
         resizable:true,
         dataIndex:'amount',
         width:100
         },
         {
         header:'tdate',
         sortable:true,
         resizable:true,
         dataIndex:'tdate',
         width:100
         },
         {
         header:'user_id',
         sortable:true,
         resizable:true,
         dataIndex:'user_id',
         width:100
         },
         {
         header:'arus',
         sortable:true,
         resizable:true,
         dataIndex:'arus',
         width:100
         },
         */
    ],
    initComponent: function () {
        if (jun.rztCustomersCmp.getTotalCount() === 0) {
            jun.rztCustomersCmp.load();
        }
        if (jun.rztReturJualTypeCmp.getTotalCount() === 0) {
            jun.rztReturJualTypeCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.store = jun.rztReturSales;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    ref: '../btnEdit'
                },
                //{
                //    xtype: 'tbseparator'
                //},
                //{
                //    xtype: 'button',
                //    text: 'Hapus',
                //    ref: '../btnDelete'
                //}
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.ReturSalesGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.ReturSalesWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.ReturSalesWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'ReturSales/delete/id/' + record.json.sales_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztReturSales.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
