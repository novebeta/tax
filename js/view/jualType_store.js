jun.JualTypestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.JualTypestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'JualTypeStoreId',
            url: 'JualType',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'akun_with_coa_id'},
                {name: 'nama_akun'},
                {name: 'type_'},
                {name: 'account_code'}
            ]
        }, cfg));
    }
});
jun.rztJualType = new jun.JualTypestore();
jun.rztJualTypeCmp = new jun.JualTypestore();
jun.rztJualTypeLib = new jun.JualTypestore();
//jun.rztJualType.load();
