jun.GlTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.GlTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GlTransStoreId',
            url: 'GlTrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'counter'},
                {
                    name: "debit",
                    type: "float"
                },
                {
                    name: "kredit",
                    type: "float"
                },
                {name: 'memo_'},
                {name: 'amount'},
                {name: 'account_code'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function() {
        Ext.getCmp("tot_debit_id").setValue(this.sum("debit"));
        Ext.getCmp("tot_kredit_id").setValue(this.sum("kredit"));
    }
});
jun.rztGlTrans = new jun.GlTransstore();
jun.JurnalUmumStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.JurnalUmumStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'JurnalUmumStoreId',
            url: 'GlTrans/ListJurnalUmum',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'type_no'},
                {name: 'tran_date'},
                {name: 'reference'},
                {name: 'tot_debit'},
                {name: 'tot_kredit'},
                {name: 'store'}
            ]
        }, cfg));
    }
});
jun.rztJurnalUmum = new jun.JurnalUmumStore();