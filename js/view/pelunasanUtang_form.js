jun.PelunasanUtangWin = Ext.extend(Ext.Window, {
    title: 'Pelunasan Utang',
    modez: 1,
    width: 590,
    height: 530,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-PelunasanUtang',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        id: 'tglid',
                        name: 'tgl',
                        fieldLabel: 'tgl',
                        format: 'd M Y',
                        width: 175,
                        x: 375,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Receipt No:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'no_bukti',
                        ref: '../no_bukti',
                        maxLength: 50,
                        width: 175,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Suplier:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        id: 'supplierid',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        ref: '../supplier',
                        fieldLabel: 'supplier_id',
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        listWidth: 350,
                        matchFieldWidth: !1,
                        width: 175,
                        x: 375,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Store:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        ref: '../cmbStore',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        allowBlank: false,
                        width: 175,
                        x: 375,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Payment:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Payment Method',
                        store: jun.rztBankCmpPusat,
                        ref: '../id_bank',
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    //{
                    //    xtype: "label",
                    //    text: "Alamat:",
                    //    x: 295,
                    //    y: 65
                    //},
                    //{
                    //    xtype: 'textarea',
                    //    ref: '../alamat',
                    //    name: 'alamat',
                    //    id: 'alamatid',
                    //    height: 50,
                    //    width: 175,
                    //    readOnly: true,
                    //    x: 375,
                    //    y: 62
                    //},
                    //{
                    //    xtype: "label",
                    //    text: "BG/Cek:",
                    //    x: 5,
                    //    y: 95
                    //},
                    //{
                    //    xtype: 'uctextfield',
                    //    fieldLabel: 'no_bg_cek',
                    //    hideLabel: false,
                    //    //hidden:true,
                    //    name: 'no_bg_cek',
                    //    id: 'no_bg_cekid',
                    //    ref: '../no_bg_cek',
                    //    width: 175,
                    //    value: '-',
                    //    x: 85,
                    //    y: 92
                    //},
                    new jun.PelunasanUtangDetilGrid({
                        x: 5,
                        y: 95,
                        height: 315,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'total',
                        hideLabel: false,
                        width: 175,
                        name: 'total',
                        id: 'totalPelunasanUtangid',
                        ref: '../total',
                        maxLength: 30,
                        value: 0,
                        x: 375,
                        y: 422
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose',
                    id: 'btnSaveClosePelunasanUtang'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PelunasanUtangWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.supplier.on('select', this.onSupplierChange, this);
        this.cmbStore.on('select', this.onSupplierChange, this);
        this.on("close", this.onWinClose, this);
    },
    onActivate: function () {
        if (this.modez == 1 || this.modez == 2) {
            Ext.getCmp("btnSaveClosePelunasanUtang").setVisible(false);
        } else {
            Ext.getCmp("btnSaveClosePelunasanUtang").setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztPelunasanUtangDetil.removeAll();
        jun.rztTagihanUtang.removeAll();
//        Ext.getCmp('form-PelunasanUtang').getForm().reset();
    },
    onSupplierChange: function () {
        this.id_bank.reset();
        var supplier_id = this.supplier.getValue();
        var store_kode = this.cmbStore.getValue();
        if (supplier_id == "" || store_kode == "") {
            return;
        }
        jun.rztTagihanUtang.baseParams = {
            supplier_id: supplier_id,
            store: store_kode
        };
        jun.rztTagihanUtang.load();
        jun.rztTagihanUtang.baseParams = {};
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'PelunasanUtang/create';
        Ext.getCmp('form-PelunasanUtang').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztPelunasanUtangDetil.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztPelunasanUtang.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PelunasanUtang').getForm().reset();
                    this.onWinClose();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});