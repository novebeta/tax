jun.TagihanUtangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TagihanUtangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TagihanUtangStoreId',
            url: 'TagihanUtang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'purchase_id'},
                {name: 'tgl'},
                {name: 'supplier_id'},
                {name: 'store'},
                {name: 'no_faktur'},
                {name: 'nilai', type: 'float'},
                {name: 'doc_ref'},
                {name: 'sisa', type: 'float'}
            ]
        }, cfg));
    }
});
jun.rztTagihanUtang = new jun.TagihanUtangstore();
//jun.rztTagihanUtang.load();
