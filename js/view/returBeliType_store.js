jun.ReturBeliTypestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReturBeliTypestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReturBeliTypeStoreId',
            url: 'ReturBeliType',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'akun_with_coa_id'},
                {name: 'nama_akun'},
                {name: 'type_'},
                {name: 'account_code'}
            ]
        }, cfg));
    }
});
jun.rztReturBeliType = new jun.ReturBeliTypestore();
jun.rztReturBeliTypeLib = new jun.ReturBeliTypestore();
jun.rztReturBeliTypeCmp = new jun.ReturBeliTypestore();
//jun.rztReturBeliType.load();
