jun.PelunasanPiutangDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PelunasanPiutangDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PelunasanPiutangDetilStoreId',
            url: 'PelunasanPiutangDetil',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pelunasan_piutang_detil'},
                {name: 'kas_dibayar', type: 'float'},
                {name: 'no_faktur'},
                {name: 'nilai', type: 'float'},
                {name: 'sisa', type: 'float'},
                {name: 'pelunasan_piutang_id'},
                {name: 'tgl'},
                {name: 'sales_id'}
            ]
        }, cfg));
    },
    refreshData: function (a) {
        var subtotal = this.sum("kas_dibayar");
        Ext.getCmp("totalPelunasanPiutangid").setValue(subtotal);
    }
});
jun.rztPelunasanPiutangDetil = new jun.PelunasanPiutangDetilstore();
//jun.rztPelunasanPiutangDetil.load();
