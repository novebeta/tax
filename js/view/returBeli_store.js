jun.ReturBelistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReturBelistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReturBeliStoreId',
            url: 'ReturBeli',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'purchase_id'},
                {name: 'doc_ref'},
                {name: 'total'},
                {name: 'ppn'},
                {name: 'akun_with_coa_id'},
                {name: 'tgl'},
                {name: 'store'},
                {name: 'tax'},
                {name: 'tax_rp'},
                {name: 'amount'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'arus'},
                {name: 'supplier_id'},
                {name: 'supplier_name'}
            ]
        }, cfg));
    }
});
jun.rztReturBeli = new jun.ReturBelistore();
//jun.rztReturBeli.load();
