jun.ReturJualTypestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReturJualTypestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReturJualTypeStoreId',
            url: 'ReturJualType',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'akun_with_coa_id'},
                {name: 'nama_akun'},
                {name: 'type_'},
                {name: 'account_code'}
            ]
        }, cfg));
    }
});
jun.rztReturJualType = new jun.ReturJualTypestore();
jun.rztReturJualTypeCmp = new jun.ReturJualTypestore();
jun.rztReturJualTypeLib = new jun.ReturJualTypestore();
//jun.rztReturJualType.load();
