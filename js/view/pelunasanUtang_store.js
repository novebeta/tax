jun.PelunasanUtangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PelunasanUtangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PelunasanUtangStoreId',
            url: 'PelunasanUtang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pelunasan_utang_id'},
                {name: 'total'},
                {name: 'no_bg_cek'},
                {name: 'no_bukti'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'bank_id'},
                {name: 'supplier_id'},
                {name: 'store'},
                {name: 'tdate'},
                {name: 'user_id'}
            ]
        }, cfg));
    }
});
jun.rztPelunasanUtang = new jun.PelunasanUtangstore();
//jun.rztPelunasanUtang.load();
