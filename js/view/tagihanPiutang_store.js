jun.TagihanPiutangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TagihanPiutangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TagihanPiutangStoreId',
            url: 'TagihanPiutang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sales_id'},
                {name: 'tgl'},
                {name: 'customer_id'},
                {name: 'store'},
                {name: 'no_faktur'},
                {name: 'nilai', type: 'float'},
                {name: 'doc_ref'},
                {name: 'sisa', type: 'float'}
            ]
        }, cfg));
    }
});
jun.rztTagihanPiutang = new jun.TagihanPiutangstore();
//jun.rztTagihanPiutang.load();
