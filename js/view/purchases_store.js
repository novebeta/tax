jun.Purchasesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Purchasesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PurchasesStoreId',
            url: 'Purchases',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'purchase_id'},
{name:'doc_ref'},
{name:'total'},
{name:'ppn'},
{name:'akun_with_coa_id'},
{name:'tgl'},
{name:'store'},
{name:'tax'},
{name:'tax_rp'},
{name:'amount'},
{name:'tdate'},
{name:'user_id'},
{name:'arus'},
{name:'supplier_id'},
                
            ]
        }, cfg));
    }
});
jun.rztPurchases = new jun.Purchasesstore();
//jun.rztPurchases.load();
