jun.PelunasanUtangDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PelunasanUtangDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PelunasanUtangDetilStoreId',
            url: 'PelunasanUtangDetil',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pelunasan_utang_detil_id'},
                {name: 'kas_dibayar', type: 'float'},
                {name: 'no_faktur'},
                {name: 'nilai', type: 'float'},
                {name: 'sisa', type: 'float'},
                {name: 'pelunasan_utang_id'},
                {name: 'tgl'},
                {name: 'purchase_id'}
            ]
        }, cfg));
    },
    refreshData: function (a) {
        var subtotal = this.sum("kas_dibayar");
        Ext.getCmp("totalPelunasanUtangid").setValue(subtotal);
    }
});
jun.rztPelunasanUtangDetil = new jun.PelunasanUtangDetilstore();