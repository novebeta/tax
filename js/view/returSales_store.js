jun.ReturSalesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReturSalesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReturSalesStoreId',
            url: 'ReturSales',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sales_id'},
                {name: 'doc_ref'},
                {name: 'total'},
                {name: 'ppn'},
                {name: 'akun_with_coa_id'},
                {name: 'tgl'},
                {name: 'store'},
                {name: 'tax'},
                {name: 'tax_rp'},
                {name: 'amount'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'arus'},
                {name: 'customer_name'},
                {name: 'customer_id'}
            ]
        }, cfg));
    }
});
jun.rztReturSales = new jun.ReturSalesstore();
//jun.rztReturSales.load();
